import React, { useEffect, useState } from 'react';
import FireBaseApp from '../../Constants/firebase';

//To-do add proper types
export const AuthContext = React.createContext(null);

export const AuthProvider = (props) => {
  const children = props.children;
  const [curUser, setcurUser] = useState(null);
  const [pending, setPending] = useState(true);
  useEffect(() => {
    FireBaseApp.auth().onAuthStateChanged((user) => {
      setcurUser(user);
      setPending(false);
    });
  }, []);

  if (pending) {
    return <h1>Loading...</h1>;
  }

  return (
    <>
      <AuthContext.Provider value={curUser}>{children}</AuthContext.Provider>
    </>
  );
};
