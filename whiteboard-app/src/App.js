import React from 'react';
import {BrowserRouter, Switch, Redirect, Route} from 'react-router-dom';
import DrawingApp from './Components/AppContainer/DrawingApp';
import ThemeProvider from './Context/Theme/ThemeContext.js'
import { AuthProvider } from './Context/AuthContext/Auth';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Context/Theme/ThemeContext.css'
import './App.css';
import NotFound from './Components/NotFound/NotFound';

const  App = () => {
    
    return (

      <BrowserRouter>
        <ThemeProvider>
          <AuthProvider>
            <Switch>
              <Redirect path="/" exact to="/draw" />
              <Route path='/draw' component={DrawingApp} />
              <Route path="/notfound" component={NotFound} />
              <Route path="*" component={NotFound} />
            </Switch>
          </AuthProvider>
        </ThemeProvider>
      </BrowserRouter>
    );
}

export default App;
