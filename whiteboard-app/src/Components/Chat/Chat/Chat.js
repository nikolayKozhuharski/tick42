//eslint:disable
import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Fab from '@material-ui/core/Fab';
import SendIcon from '@material-ui/icons/Send';
import * as firebase from "firebase"
import "./Chat.css"


const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  chatSection: {
    width: '100%',
    height: '80vh'
  },
  headBG: {
    backgroundColor: '#e0e0e0'
  },
  borderRight500: {
    borderRight: '1px solid #e0e0e0'
  },
  messageArea: {
    height: '70vh',
    overflowY: 'auto'
  },
  closeButton: {
    width: '15px',
    marginLeft: 'auto',
    cursor: 'pointer'
  }
});


function getUniqueUsers(messages) {
  const users = new Map();
  messages.forEach((message) => {
    if (!users.has(message.uid)) {
      users.set(message.uid, {
        uid: message.uid,
        email: message.email
      });
    }
  });

  return users;
}

const Chat = (props) => {
  const classes = useStyles();
  const [chatMessages, setChatMessages] = useState([]);
  const [users, setUsers] = useState(new Map());
  const [input, setInput] = useState('');

  const boardId = window.location.pathname.split('/').pop();
  const handleClose = props.handleClose;

  useEffect(() => {
    const newUsers = getUniqueUsers(chatMessages);
    if (newUsers.size > users.size) {
      const promises = [];
      newUsers.forEach((user, uid) => {
        if (users.has(uid)) {
          user.photo = users.get(uid).photo;
        } else {
          promises.push(firebase.storage()
            .ref(`ProfilePics/${user.uid}`)
            .getDownloadURL().then((url) => {
              user.photo = url;
            })
          );
        }
      });

      Promise.all(promises).then(() => {
        setUsers(newUsers);
      });
    }
  }, [chatMessages, users])

  const db = firebase.firestore();

  const chatHistoryPath = `boards/${boardId}/chat`;
  const chatHistory = db.collection(chatHistoryPath);


  const user = firebase.auth().currentUser;
  const userEmail = user.email;
  const userId = user.uid;

  const loadingMessages = () => {
    const updatedMessage = {
      message: input,
      uid: userId,
      email: userEmail,
      time: firebase.firestore.Timestamp.now(),
    }
    chatHistory.doc().set(updatedMessage)
    setInput('');
  }

  const dummyScrolTargetRef = useRef(null);


  const scrollToBottom = () => {
    dummyScrolTargetRef.current && dummyScrolTargetRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    db.collection("boards")
      .doc(boardId)
      .collection("chat")
      .orderBy("time", "asc")
      .onSnapshot(function (data) {
        let allMessages = []
        data.forEach(function (newMessage) {
          allMessages.push(newMessage.data())
        })
        setChatMessages([...allMessages])
      });
  }, [boardId, db]);

  useEffect(() => {
    scrollToBottom();
  }, [chatMessages])

  const sendMessageOnEnter = (e) => {
    if (e.key === 'Enter') {
      loadingMessages();
    }
  }


  const storyEmail = [];
  chatMessages.forEach((e) => storyEmail.push(e.email))

  const uniqueEmailinBoard = [...new Set(storyEmail)];


  return (
    <div style={{ width: "80rem" }}>
      <Grid container>
        <Grid item xs={11} >
          <Typography variant="h5" className="header-message">Chat</Typography>
        </Grid>
        <div className={classes.closeButton} onClick={handleClose}>
          X
        </div>
      </Grid>
      <Grid container component={Paper} className={classes.chatSection}>
        <Grid item xs={3} className={classes.borderRight500}>
          <List> {Array.from(users.values()).map((user, i) =>
            <ListItem key={i}>
              <ListItemIcon>
                <Avatar alt={user.email} src={user.photo} />
              </ListItemIcon>
              <ListItemText >{user.email}</ListItemText>
            </ListItem>
          )}
          </List>
        </Grid>
        <Grid item xs={9}>
          <List className={classes.messageArea}>
            {chatMessages.map((m, i) => <ListItem className="bubble-container" key={i}>
              <Grid container className={"bubble " + (userEmail === m.email ? "bubble-right" : "bubble-left")}>
                <Grid item xs={12}>
                  <ListItemText secondary={m.email}></ListItemText>
                </Grid>
                <Grid item xs={12}>
                  <ListItemText secondary={m.time.toDate().toGMTString()}></ListItemText>
                </Grid>
                <Grid item xs={12}>
                  <ListItemText primary={m.message}></ListItemText>
                </Grid>
              </Grid>
            </ListItem>)}
            <div ref={dummyScrolTargetRef}></div>
          </List>
          <Grid container style={{ padding: '20px' }}>
            <Grid item xs={11}>
              <TextField id="outlined-basic-email" label="WhiteBoardChat" type='text' fullWidth value={input} onKeyPress={sendMessageOnEnter} onChange={(e) => { setInput(e.target.value) }} />
            </Grid>
            <Grid xs={1} className="right">
              <Fab color="primary" aria-label="add" onClick={loadingMessages}><SendIcon /></Fab>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
export default Chat;