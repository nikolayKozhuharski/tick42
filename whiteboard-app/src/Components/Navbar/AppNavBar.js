import React, {useContext, useEffect, useState} from 'react';
import firebase from 'firebase';

import {Navbar, Nav, Form, Button, Dropdown, InputGroup, ButtonGroup} from 'react-bootstrap'
import AppColorPicker from '../ColorPicker/AppColorPicker'
import {Tools} from 'react-sketch';
import AppNavItem from './Popups/AppOverlay';
import { ThemeContext } from '../../Context/Theme/ThemeContext.js';

import Brightness3Icon from '@material-ui/icons/Brightness3';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import ZoomOutMapIcon from '@material-ui/icons/ZoomOutMap';

import pointer_active_light from '../../static/img/pointer_active_light.png';
import pointer_active_dark from '../../static/img/pointer_active_dark.png';
import pointer_disabled_light from '../../static/img/pointer_disabled_light.png';
import pointer_disabled_dark from '../../static/img/pointer_disabled_dark.png';

import {withRouter} from 'react-router';
import ChatButton from '../Chat/ChatButton';
const AppNavbar = ({setTool, 
    setLineWidth, 
    lineWidth,
    sketch, 
    text, 
    setText, 
    lineColor, 
    setLineColor, 
    backgroundColor, 
    image, 
    setImage, 
    canRedo, 
    canUndo, 
    setCanRedo, 
    setCanUndo,
    fillColor,
    setFillColor,
    setBackgroundColor,
    updateFireStore,
    boardHistoryStr,
    boardId,
    setZoomBoardLevel,
    zoomBoardLevel,
    enableMousePointer,
    setEnableMousePointer,
    history}) => {

    const [boardName, setBoardName] = useState('');

    const {theme, changeTheme} = useContext(ThemeContext);

//I thought of moving these to a seperate folder however we wont have enough time -_- 

    const selectPencil = () => {

        setTool(Tools.Pencil)
    };
    
    const selectRect = () => {
    
        setTool(Tools.Rectangle)
    };

    const selectCircle = () => {
        setTool(Tools.Circle);
    };
    
    const selectPan = () => {
        setTool(Tools.Pan);
    };

    const selectSelect = () => {
        setTool(Tools.Select)
    };
    
    const selectLine = () => {
        setTool(Tools.Line)
    };

    const formLineWidth = (e) => {
        const val = e.target.value;
        setLineWidth(+val)
    };

    const setTransFill = () => {
        setFillColor('transparent')
    };

    const undo =async () => {
        sketch.undo();
        setCanUndo(sketch.canUndo());
        setCanRedo(sketch.canRedo());
        await updateFireStore(boardHistoryStr, sketch.toJSON())

    };

    const redo = async () => {
        sketch.redo();
        setCanRedo(sketch.canRedo());
        setCanUndo(sketch.canUndo());
        await updateFireStore(boardHistoryStr, sketch.toJSON())

    };

    const addImg = async () => {
        await sketch.addImg(image)
        setTimeout(()=>
        updateFireStore(boardHistoryStr, sketch.toJSON()), 0)
    };
    const addText = async (e) => {
        e.preventDefault();
        await sketch.addText(text)
        await updateFireStore(boardHistoryStr, sketch.toJSON())
    };
    const setCanvasColor = async (color) => {
        await setBackgroundColor(color);
        await updateFireStore(boardHistoryStr, sketch.toJSON())
    };

    const handleBoardZoomOut = () => {
        if (zoomBoardLevel <= 25) return;
        setZoomBoardLevel(zoomBoardLevel - 25);
      };
    
    const handleBoardZoomIn = () => {
        if (zoomBoardLevel >= 500) return;
        setZoomBoardLevel(zoomBoardLevel + 25);
    };
    
    const handleBoardZoomNormal = () => {
        setZoomBoardLevel(100);
    };

    const toggleMousePointer = () => {
        setEnableMousePointer(!enableMousePointer);
      };

    useEffect(()=>{
        const boardDoc= firebase.firestore().collection(`boards`).doc(boardId);
        boardDoc.get().then((doc) => setBoardName(`Name: ${doc.data().name}`))
          .catch(() => history.push('/notfound'));
    },[boardId, history])

    return(
        <Navbar sticky='top' style={{border: '0.1rem solid black',background: 'linear-gradient(90deg, rgba(89,179,196,1) 0%, rgba(112,124,166,1) 47%, rgba(138,87,154,1) 100%)'}} expand="lg">
            <Navbar.Brand className={`text-top nav-item-text-${theme.background}`}>Drawing Board {boardName}</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <AppNavItem id='Tools' name='Tools'>                                
                        <Dropdown>
                            <Dropdown.Toggle variant="light" id="dropdown-Tools">
                                Tools
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Item onClick={selectPencil}>Pencil</Dropdown.Item>
                                <Dropdown.Item onClick={selectLine}>Line</Dropdown.Item>
                                <Dropdown.Item onClick={selectRect}>Rectangle</Dropdown.Item>
                                <Dropdown.Item onClick={selectCircle}>Circle</Dropdown.Item>
                                <Dropdown.Item onClick={selectPan}>Pan</Dropdown.Item>
                                <Dropdown.Item onClick={selectSelect}>Select</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                        <hr />
                        <Form>
                            <Form.Group >
                                <Form.Label className={`nav-item-text-${theme.text}`}>Line Width</Form.Label>
                                <Form.Control onChange={formLineWidth} defaultValue={lineWidth} id='lineWeight' type="range" />
                            </Form.Group>
                        </Form>
                        <hr />
                        <Form onSubmit={addText}>
                            <Form.Group className='mx-auto'>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <Button  variant={`outline-${theme.text}`} type='submit'>
                                            Set text
                                        </Button>
                                    </InputGroup.Prepend>
                                    <Form.Control id='canvasDimension' type="text" value={text} onChange={(e) => setText(e.target.value)} />
                                </InputGroup>

                            </Form.Group>
                        </Form>
                    </AppNavItem>
                    <AppNavItem id='Drawing Colors' name='Drawing Colors'>
                        <p className={`nav-item-text-${theme.text}`}>Line Color</p>
                        <AppColorPicker className={'mb-3'} color={lineColor} onChangeComplete={setLineColor}/>
                        <hr />
                        <p className={`nav-item-text-${theme.text}`}>Fill Color</p>
                        <AppColorPicker color={fillColor} onChangeComplete={setFillColor}/>
                        <Button className='m-1' variant={`outline-${theme.text}`} onClick={setTransFill} > Transparent </Button>
                    </AppNavItem> 
                    <AppNavItem id='Canvas Color' name='Canvas Color'>
                        <AppColorPicker  color={backgroundColor} onChangeComplete={setCanvasColor}/>
                    </AppNavItem>
                    <AppNavItem id='Image' name='Image'>
                        <Form>
                            <Form.Group className='mx-auto'>
                                <Form.Control id='canvas-image' type="text" value={image} onChange={(e) => setImage(e.target.value)} />
                                <Button  className='mt-2' variant={`outline-${theme.text}`} onClick={addImg}>
                                    Load Image
                                </Button>
                            </Form.Group>
                        </Form>
                    </AppNavItem>
                    <AppNavItem id='BoardOptions' name='Board Options'>
                        <p className={`nav-item-text-${theme.text}`}>Zoom</p>
                        <hr className={`nav-item-text-${theme.text}`} />
                        <ZoomOutIcon
                            onClick={handleBoardZoomOut}
                            style={{ marginLeft: 10, cursor: 'pointer' }}
                            className={`nav-item-text-${theme.text}`}
                            />
                        <label
                            style={{ marginLeft: 2, marginTop: 6 }}
                            className={`nav-item-text-${theme.text}`}
                            >
                            {zoomBoardLevel < 100
                                ? -1 * zoomBoardLevel
                                : zoomBoardLevel}
                        </label>
                        <ZoomInIcon
                            onClick={handleBoardZoomIn}
                            style={{ marginLeft: 2, cursor: 'pointer' }}
                            className={`nav-item-text-${theme.text}`}
                            />
                        <ZoomOutMapIcon
                            onClick={handleBoardZoomNormal}
                            style={{ marginLeft: 2, cursor: 'pointer' }}
                            className={`nav-item-text-${theme.text}`}
                            />
                            <hr className={`nav-item-text-${theme.text}`} />
                      
                            <ButtonGroup>
                            <Button className={`m-1`} variant={`outline-${theme.text}`} onClick={changeTheme}>{theme.background === 'light' ? <WbSunnyIcon /> : <Brightness3Icon />}</Button>

                            
                            </ButtonGroup>

                    </AppNavItem>

                    <Button className='m-1' onClick={undo} 
                    disabled={!canUndo} 
                    variant={`outline-${theme.background}`}>Undo</Button>

                    <Button className='m-1' onClick={redo} 
                    disabled={!canRedo}
                    variant={`outline-${theme.background}`}>Redo</Button>
                <span className="mouse-pointer-menu">
            <span>
              <img
                id="img_check"
                src={
                  enableMousePointer
                    ? theme.text === 'dark'
                      ? pointer_active_light
                      : pointer_active_dark
                    : theme.text === 'dark'
                    ? pointer_disabled_light
                    : pointer_disabled_dark
                }
                onClick={toggleMousePointer}
                styele={{
                    maxWidth: '1rem',
                    maxHeight: '1rem'
                }}
                className="mouse-pointer-check"
                alt='Mouse'
              />
            </span>
          </span>

          <Button className={`m-1`}  variant={`outline-${theme.background}`} >
            <ChatButton />
        </Button>
                    
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default withRouter(AppNavbar);