import React, {useContext} from 'react'
import {Button, OverlayTrigger, Popover} from 'react-bootstrap'
import { ThemeContext } from '../../../Context/Theme/ThemeContext.js';

const AppOverlay = (props) => {
    const {theme} = useContext(ThemeContext)
    return (
        <OverlayTrigger 
        trigger="click" 
        rootClose
        placement="bottom" 
        overlay={
            <Popover id={`popover-${props.name}`} className={`bg-${theme.background}`}>
                <Popover.Title as="h4" className={`bg-${theme.background}`}>{props.name}</Popover.Title>
                <Popover.Content>
                    {props.children}
                </Popover.Content>
            </Popover>
        }>
            <Button className='m-1' variant={`outline-${theme.background}`}>{props.name}</Button>
        </OverlayTrigger> 
    );
};

export default AppOverlay;