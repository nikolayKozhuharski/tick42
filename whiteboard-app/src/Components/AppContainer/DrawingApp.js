import React, {useEffect, useState} from 'react';
import {SketchField, Tools} from 'react-sketch';
import AppNavbar from '../Navbar/AppNavBar';
import firebase from 'firebase'
import {withRouter} from 'react-router';
import Footer from '../Footer/Footer.js';

import MousePointers from '../MousePointers/MousePointers.js'

const DrawingApp = (props) => {
    const [sketchRef, setSketchRef] = useState(null);
    const setRef = (ref) => (setSketchRef(ref));
    const [lineWidth, setLineWidth] = useState(10);
    const [lineColor, setLineColor] = useState('black');
    const [fillColor, setFillColor] = useState('#68CCCA');
    const [backgroundColor, setBackgroundColor] = useState('#FFFFFF');
    const [canUndo, setCanUndo] = useState(false);
    const [canRedo, setCanRedo] = useState(false);
    const [imageUrl, setImageUrl] = useState('https://files.gamebanana.com/img/ico/sprays/4ea2f4dad8d6f.png');
    const [text, setText] = useState('a text, cool!');
    const [tool, setTool] = useState(Tools.Pencil);
    const [canvasVal, setCanvasVal] = useState({});

    const [enableMousePointer, setEnableMousePointer] = useState(false);
    const [zoomBoardLevel, setZoomBoardLevel] = useState(100);
    const [mousePointer, setMousePointer] = useState({ x: 0, y: 0 });

    const boardId = props.location.pathname.split('/').pop();
    const boardHistoryStr = `boards/${boardId}/history`;
    const updateFireStore = async (path, json) => {
        const user = firebase.auth().currentUser;
        
        const boardHistory = firebase
        .firestore()
        .collection(path); 
        
        const updatedBoard = {
            canvas: JSON.stringify(json),
            time: firebase.firestore.FieldValue.serverTimestamp(),
            user: user.uid
        };
        await boardHistory.doc().set(updatedBoard);
    };
    const checkUndoRedo = () => {
        let prev = canUndo;
        let now = sketchRef.canUndo();
        if (prev !== now) {
          setCanUndo(now);
        }
    };
    const getFireStoreBoard = async (path) => {
        const boardHistory = firebase
        .firestore()
        .collection(path); 

        boardHistory
        .orderBy('time', 'desc')
        .limit(1)
        .onSnapshot(function (snapshot) {
            snapshot.forEach(function (newBoard) {
                setCanvasVal(JSON.parse(newBoard.data().canvas))
            });
          });
          console.log('from EFFECT')

    };
    const onChange = (e) => {
        if(e === undefined){
            return ;
        }
        if(e.type === 'mouseup'){
            console.log(e)
            updateFireStore(boardHistoryStr, sketchRef.toJSON())
        }
        checkUndoRedo()
    };

    const onMouseMove = (e) => {
        setMousePointer({ x: e.clientX, y: e.clientY });
    };


    useEffect(() => {
        getFireStoreBoard(boardHistoryStr)
    }, [boardHistoryStr])


    return(
        <>
            <AppNavbar  setTool={setTool} 
                        setLineWidth={setLineWidth}
                        lineWidth={lineWidth}
                        sketch={sketchRef} 
                        text={text}
                        setText={setText}
                        lineColor={lineColor}
                        setLineColor={setLineColor}
                        backgroundColor={backgroundColor}
                        image={imageUrl}
                        setImage={setImageUrl}
                        canUndo={canUndo}
                        canRedo={canRedo}
                        setCanRedo={setCanRedo}
                        setCanUndo={setCanUndo}
                        fillColor={fillColor}
                        setBackgroundColor={setBackgroundColor}
                        setFillColor={setFillColor}
                        boardHistoryStr={boardHistoryStr}
                        updateFireStore={updateFireStore}
                        boardId={boardId}
                        setZoomBoardLevel={setZoomBoardLevel}
                        zoomBoardLevel={zoomBoardLevel}
                        enableMousePointer={enableMousePointer}
                        setEnableMousePointer={setEnableMousePointer}/>
<br />
            <div  style={{
                border: 'solid 3px black',
                transform: `scale(${Math.abs(zoomBoardLevel) / 100})`
            }}
            onMouseMove={onMouseMove}>

                <SketchField 
                        width={window.width}
                        height={window.height}
                        ref={setRef}
                        tool={tool} 
                        lineColor={lineColor}
                        lineWidth={lineWidth}
                        onChange={onChange}
                        backgroundColor={backgroundColor}
                        fillColor={fillColor}
                        value={canvasVal}/>
            </div>
            <MousePointers
                boardId={boardId}
                mousePointer={mousePointer}
                zoomBoardLevel={zoomBoardLevel}
                enableMousePointer={enableMousePointer}
      />
            <Footer />
        </>
    )
}

export default withRouter(DrawingApp);