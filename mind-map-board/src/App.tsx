import React, { useState } from 'react';
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import { AuthProvider } from './Context/AuthContext/Auth';
import NotFound from './components/NotFound/NotFound';
import './static/css/App.css';
import ThemeProvider from './Context/Theme/ThemeContext';
import ReactNotifications from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import Navigation from './components/Navigation/Navigation';
import Footer from './components/Footer/Footer';
import MindMap from './components/MindMapComponent/MindMap';
import ChatButton from './components/Chat/ChatButton';

function App() {
  const [zoomBoardLevel, setZoomBoardLevel] = useState(100);
  const [enableMousePointer, setEnableMousePointer] = useState(false);

  return (
    <BrowserRouter>
      <ReactNotifications />
      <ThemeProvider>
        <AuthProvider >
          {/* <Navigation zoomBoardLevel={zoomBoardLevel} setZoomBoardLevel={setZoomBoardLevel}
          enableMousePointer={enableMousePointer} setEnableMousePointer={setEnableMousePointer} /> */}
          <div id="app">
            <Switch>
              <Redirect path="/" exact to="/mindmap" />
              <Route path="/mindmap" render={(props) => (
                <MindMap {...props} zoomBoardLevel={zoomBoardLevel} enableMousePointer={enableMousePointer} />
              )} />
              {/* <MindMap /> */}
              <Route path="*" component={NotFound} />
            </Switch>
          </div>
         
          {/* <Footer /> */}
        </AuthProvider >
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
