import React from "react";
import cx from "classnames";
import "./Toolbar.css";
import { iconClassName } from "@blink-mind/renderer-react";
import { ToolbarItemTheme } from "./toolbar-item-theme";
import { ToolbarItemSearch } from "./toolbar-item-search";
import { SaveButton } from "./SaveButton";
import ChatButton from "../../Chat/ChatButton";

export const Toolbar = (props: any) => {
  const { onClickUndo, onClickRedo, canUndo, canRedo } = props;

  return (
    <div className="bm-toolbar mindMapToolbar">
      <SaveButton {...props}/>
      <ChatButton />
      <div
        className={cx("bm-toolbar-item", iconClassName("undo"), {
          "bm-toolbar-item-disabled": !canUndo,
        })}
        onClick={onClickUndo}
      />

      <div
        className={cx("bm-toolbar-item", iconClassName("redo"), {
          "bm-toolbar-item-disabled": !canRedo,
        })}
        onClick={onClickRedo}
      />
    </div>
  );
};
