import React, { useState, useEffect, useRef } from "react";
import { Diagram } from "@blink-mind/renderer-react";
import { Toolbar } from "./toolbar/Toolbar";
import { generateSimpleModel } from "../MindMapComponent/utils";
import "@blink-mind/renderer-react/lib/main.css";
import "./MindMap.css"
import { useStateCallback } from "../MindMapComponent/utils/useStateCallback";
import { JsonSerializerPlugin } from "@blink-mind/plugin-json-serializer";
import { useCallbackRef } from 'use-callback-ref';
import * as firebase from "firebase";
import { Model } from "@blink-mind/core";

const plugins = [
  JsonSerializerPlugin()
]
export default function MindMap(props: any) {
  const [model, setModel] = useStateCallback<any>(Model.create({ topics: [{ key: 'dummy', blocks: [] }], rootTopicKey: 'dummy' }));
  const [, updateState] = React.useState();

  const diagramRef = useRef<any>(null);
  const diagramRefWithCallback = useCallbackRef(null, ref => {
    const prevDiagramRef = diagramRef.current;
    diagramRef.current = ref;
    if (!prevDiagramRef) {
      updateState({} as any);
    }
  });

  function renderDiagram() {
    return <Diagram ref={diagramRefWithCallback} model={model} onChange={onChange} plugins={plugins} />;
  };

  useEffect(() => {
    if (diagramRef.current) {
      let model;

      const Id = window.location.pathname.split("/").pop();
      const db = firebase.firestore();
      const dbBoard = db.collection("boards").doc(Id);
      dbBoard.get().then((board: any) => {

        const serializedModel = board.data().model;

        if (!serializedModel) {
          model = generateSimpleModel();
        } else {
          const diagramProps = diagramRef.current!.getDiagramProps();
          const { controller } = diagramProps;
          let obj = JSON.parse(serializedModel);
          model = controller.run("deserializeModel", { controller, obj });
        }

        setModel(model);

      });

    }
  }, [diagramRef.current]);


  const onClickUndo = (e: any) => {
    const prop = diagramRef.current!.getDiagramProps();
    const { controller } = prop;
    controller.run("undo", prop);
  };

  const onClickRedo = (e: any) => {
    const props = diagramRef.current.getDiagramProps();
    const { controller } = props;
    controller.run("redo", props);
  };

  const onChange = (model: any, ) => {
    setModel(model);
  };

  const renderToolbar = () => {
    const props = diagramRef.current.getDiagramProps();
    const { controller } = props;
    const canUndo = controller.run("canUndo", props);
    const canRedo = controller.run("canRedo", props);
    const toolbarProps = {
      diagram: diagramRef.current,
      onClickUndo,
      onClickRedo,
      canUndo,
      canRedo,
    };
    return <Toolbar {...toolbarProps} />;
  };

  if (!model) {
    return null;
  }

  return (
    <div className="mindmap">
      {diagramRef.current && renderToolbar()}
      {renderDiagram()}
    </div>
  );
}
