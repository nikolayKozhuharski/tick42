import React, { useEffect, useState } from "react";
import * as firebase from "firebase";


export function useModel(props: any) {
  const [model, setModel] = useState(null)
  const Id = window.location.pathname.split("/").pop();
  const db = firebase.firestore();
  const { diagram } = props;
  const diagramProps = diagram.getDiagramProps();
  const { controller } = diagramProps;

  useEffect(() => {
    const Id = window.location.pathname.split("/").pop();
    const db = firebase.firestore();
    const { diagram } = props;
    const diagramProps = diagram.getDiagramProps();
    const { controller } = diagramProps;
    const dbBoard = db.collection("boards").doc(Id);
    dbBoard.get().then((txt: any) => {
      let obj = JSON.parse(txt);
      let model = controller.run("deserializeModel", { controller, obj });
      setModel(model);
      // diagram.openNewModel(model);
    });
  }, []);
  return model;
}
