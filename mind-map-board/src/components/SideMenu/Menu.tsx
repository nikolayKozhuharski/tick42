import React from 'react';
import '../../static/css/Menu.css';

const Menu = (props: any) => {
  const { x, y }: any = props;
  const menuStyle = {
    top: y,
    left: x,
  };

  return (
      <div style={menuStyle} className="menu">
        {props.children}
      </div>
    )

}

export default Menu;