## Public Part  - 100%  

- The public part of the project <span style="color: red">MUST</span> be visible without authentication.   
- Application <span style="color: red">MUST</span> have a landing page.  
- User <span style="color: red">MUST</span> be able to register/login.  
  - The authentication <span style="color: orange">SHOULD</span> be token based.  
- User <span style="color: orange">SHOULD</span> be able to recover password.   

## Private Part  - 98%  

- The private part of the system <span style="color: red">MUST</span> be accessible after a successful authentication.  - done in firebase and main app. need to add ProtectedRoute in the boards for more userfriendly experience. Now unautorized users are send to NotFound page.  
- Application <span style="color: red">MUST</span> have a public boards page.   
- Clicking on a board <span style="color: red">MUST</span> open the board in its latest state.  

## Administration Part - 80%    

- Application <span style="color: red">MUST</span> have a boards management page. 
  - User <span style="color: red">MUST</span> be able to create/delete boards. 
  - User <span style="color: orange">SHOULD</span> be able to rename boards.  
- User <span style="color: orange">SHOULD</span> be able to change password.  
- Application <span style="color: orange">SHOULD</span> have a private boards page.  
    - Private boards <span style="color: red">MUST</span> be accessible only by the user that created them.  
  - User <span style="color: red">MUST</span> be able to toggle the board visibility between public and private inside the boards management page.  
  - <span style="color: red">User MUST be displayed an error page when trying to access a private board of another user</span> TODO  

## Boards  

- Board state <span style="color: red">MUST</span> be preserved   
- After each change (e.g. adding a Kanban card) the board state <span style="color: red">MUST</span> be updated and saved.  
- Opening a board <span style="color: red">MUST</span> display its latest state. 

### Kanban board

- User <span style="color: red">MUST</span> be able to create Kanban boards.   
- User <span style="color: red">MUST</span> be able to add/delete columns.  
- User <span style="color: red">MUST</span> be able to add/delete cards.  
- User <span style="color: red">MUST</span> be able to rename columns/cards.  
- User <span style="color: red">MUST</span> be able to drag and drop cards between columns.  
- User <span style="color: orange">SHOULD</span> be able to assign colors to columns/cards.  
- User <span style="color: orange">SHOULD</span> be able to assign cards to users.  
- User <span style="color: orange">SHOULD</span> be able to assign planned start and due dates to cards.    
   
### Drawing board

- User <span style="color: green">COULD</span> be able to draw   
- User <span style="color: red">MUST</span> be able to draw using a pencil and a brush   
- User <span style="color: red">MUST</span> be able to change the color of the pencil/brush    
- User <span style="color: red">MUST</span> be able to erase    
- User <span style="color: red">MUST</span> be able to create text boxes    
- User <span style="color: orange">SHOULD</span> be able to format the text of the text boxes    
- User <span style="color: orange">SHOULD</span> be able to change the thickness of the pencil/brush   


### Database board

- User <span style="color: red">MUST</span> be able to design databases  

-	User <span style="color: red">MUST</span> be able to add/delete tables  
-	User <span style="color: red">MUST</span> be able to add/delete fields  
-	User <span style="color: red">MUST</span> be able to assign relationships (1 to 1, 1 to many and many to many) between tables  
-	User <span style="color: red">MUST</span> be able to put constraints on fields  
-	User <span style="color: orange">SHOULD</span> be able to assign colors to tables/fields  
-	User <span style="color: green">COULD</span> be able to auto arrange the tables  

## Other
-	User <span style="color: orange">SHOULD</span> be able to toggle between light and dark color theme   
-	User <span style="color: orange">SHOULD</span> be able to collaborate with other team members  
Hint: Use WebSockets for the client-server communication.  
-	User <span style="color: red">MUST</span> be able to invite other people to the board (e.g. via shareable links)  
-	User <span style="color: orange">SHOULD</span> be able to see the other team members' mouse pointers  
-	User <span style="color: orange">SHOULD</span> be able to chat with other team members 
- User <span style="color: orange">SHOULD</span> be able to zoom in and out  
  - User <span style="color: red">MUST</span> be displayed the current zoom level  
- Application <span style="color: green">COULD</span> support    

- <span style="color: red">MUST</span>:
-	Use ReactJS
-	Create a beautiful and responsive UI  
-	Use Git for source control
-	Use GitHub
  - Take advantage of branches for the development of the different features  
-	Pass the default ESLint react-app linting configuration without any errors  - 40%  



## Not Done  

- User <span style="color: orange">SHOULD</span> be able to export the Kanban board as a Gantt chart.  
- User <span style="color: orange">SHOULD</span> be able to receive notifications (e.g. when mentioned inside a comment by another team member)  
- User <span style="color: green">COULD</span> be able to export a board as a PDF   
- User <span style="color: green">COULD</span> be able to backup and restore backups of boards  
- User <span style="color: green">COULD</span> be able to use keyboard shortcuts for different functionalities (e.g. switch from pencil to brush and vice versa)  
- User <span style="color: green">COULD</span> be able to review activity history  
- User <span style="color: green">COULD</span> be able to undo and redo actions  
