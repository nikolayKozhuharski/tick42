/// <reference types="react-scripts" />
declare module 'react-dark-mode-toggle';
declare module 'react-color';

declare module 'react-trello';
declare module 'react-trello/dist/widgets/DeleteButton';
declare module 'react-trello/dist/styles/Base';
declare module 'react-trello/dist/widgets/InlineInput';