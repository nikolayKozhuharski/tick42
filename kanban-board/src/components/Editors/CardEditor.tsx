import React, { useState, useEffect } from 'react';
import { store } from 'react-notifications-component';
import { GithubPicker } from 'react-color';
import '../../static/css/Menu.css';
import { Button } from 'react-bootstrap';

const CardEditor = (props: any): any => {
  const [title, setTitle] = useState(props.card.title);
  const [assignee, setAssignee] = useState(props.card.assignee);
  const [startOn, setStartOn] = useState(props.card.startOn);
  const [dueOn, setDueOn] = useState(props.card.dueOn);
  const [cardColor, setCardColor] = useState(props.card.cardColor);
  const [label, setLabel] = useState(props.card.label);

  const handleEdit = (): void => {
    if (title.length < 1 || title.length > 25) {
      store.addNotification({
        title: 'Error',
        message: 'Card title must be between 1 to 25 symbols!',
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
      return;
    }
    if (startOn && dueOn && startOn > dueOn) {
      store.addNotification({
        title: 'Error',
        message: 'Start Date must be before Due on!',
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
      return;
    }
    if (label.length > 35) {
      store.addNotification({
        title: 'Error',
        message: 'Label must be below 35 symbols!',
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
      return;
    }
    if (assignee !== '' && !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(assignee)) {
      store.addNotification({
        title: 'Error',
        message: 'Assignee must be empty or valid e-mail!',
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
      return;
    }
    props.onEdit({
      ...props.card,
      title: title,
      startOn: startOn,
      dueOn: dueOn,
      assignee: assignee,
      cardColor: cardColor,
      label: label,
    });
  };

  // Update Card Fields
  useEffect(() => {
    setTitle(props.card.title);
    setAssignee(props.card.assignee);
    setStartOn(props.card.startOn);
    setDueOn(props.card.dueOn);
    setCardColor(props.card.cardColor);
    setLabel(props.card.label);
  }, [props]);

  return (
    <div
      style={{
        background: 'transparent',
      }}
    >
      <div style={{ padding: 5, margin: 5 }}>
        <div>
          <div style={{ marginBottom: 5 }}>
            <div className="card-settings-title">Card settings:</div>
            <div>
              <div className="menu-labels">Title: </div>
              <input
                className="form-control form-control-side-menu"
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                placeholder="Title"
              />
            </div>
            <div>
              <div className="menu-labels">Assignee email: </div>
              <input
                className="form-control form-control-side-menu"
                type="text"
                value={assignee}
                onChange={(e) => setAssignee(e.target.value)}
                placeholder="Assignee"
              />
            </div>
            <div>
              <div className="menu-labels">Start Date: </div>
              <input
                className="form-control form-control-side-menu"
                type="date"
                value={startOn}
                onChange={(e) => setStartOn(e.target.value)}
              />
            </div>
            <div>
              <div className="menu-labels">End Date: </div>
              <input
                className="form-control form-control-side-menu"
                type="date"
                value={dueOn}
                onChange={(e) => setDueOn(e.target.value)}
              />
            </div>
            <div>
              <div className="menu-labels">Label text: </div>
              <input
                className="form-control form-control-side-menu"
                type="text"
                value={label}
                onChange={(e) => setLabel(e.target.value)}
                placeholder="Label"
              />
            </div>
            <div className="menu-labels">
              <span>Label color: </span>
              <input
                type="color"
                value={cardColor}
                onChange={(e) => setCardColor(e.target.value)}
              />
              <GithubPicker
                onChange={(color: any) => setCardColor(color.hex)}
                color={cardColor}
                width= "213px"
              />
            </div>
          </div>
        </div>
        <Button className="btn btn-primary btn-card-edit" onClick={handleEdit}>
          Update
        </Button>
      </div>
    </div>
  );
};

export default CardEditor;
