import React, { useState, useEffect } from 'react';
import { store } from 'react-notifications-component';
import { GithubPicker } from 'react-color';
import '../../static/css/Menu.css';
import { Button } from 'react-bootstrap';

const LaneEditor = (props: any): any => {
  const [title, setTitle] = useState(props.lane.title);
  const [laneColor, setLaneColor] = useState(props.lane.laneColor);

  const handleEdit = (): void => {
    if (title.length > 0 && title.length < 25) {
      props.onEdit({
        ...props.lane,
        title: title,
        laneColor: laneColor,
      });
    } else {
      store.addNotification({
        title: 'Error',
        message: 'Lane title must be between 1 to 25 symbols!',
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
    }
  };

  // Update Lane Fields
  useEffect(() => {
    setTitle(props.lane.title);
    setLaneColor(props.lane.laneColor);
  }, [props]);

  return (
    <div
      style={{
        background: 'transparent',
      }}
    >
      <div style={{ padding: 5, margin: 5 }}>
        <div>
          <div style={{ marginBottom: 5 }}>
            <div className="lane-settings-title">Lane settings:</div>
            <div>
              <div className="menu-labels">Title: </div>
              <input
                className="form-control form-control-side-menu"
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                placeholder="Title"
              />
            </div>
            <div className="menu-labels">
              <span>Label color: </span>
              <input
                type="color"
                value={laneColor}
                onChange={(e) => setLaneColor(e.target.value)}
              />
              <GithubPicker
                onChange={(color: any) => setLaneColor(color.hex)}
                color={laneColor}
                width="213px"
              />
            </div>
          </div>
        </div>
        <Button className="btn btn-primary lane-edit" onClick={handleEdit}>
          Update
        </Button>
      </div>
    </div>
  );
};

export default LaneEditor;
