import React, { useContext } from 'react';
import '../../static/css/Menu.css';
import { ThemeContext } from '../../Context/Theme/ThemeContext';

const Menu = (props: any) => {
  const { theme } = useContext(ThemeContext);
  
  const { x, y }: any = props;
  const menuStyle = {
    top: y,
    left: x,
  };

  return (
    <div style={menuStyle} className={`bg-${theme.background} menu`}>
      {props.children}
    </div>
  );
};

export default Menu;
