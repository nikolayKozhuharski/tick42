import React, { useState, useEffect, useContext } from 'react';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import '../../constants/base';
import Board from 'react-trello';
import KanbanCard from '../CustomComponents/KanbanCard';
import KanbanNewCard from '../CustomComponents/KanbanNewCard';
import CustomLaneHeader from '../CustomComponents/KanbanLaneHeader';
import CardEditor from '../Editors/CardEditor';
import LaneEditor from '../Editors/LaneEditor';
import KanbanNewLaneForm from '../CustomComponents/KanbanNewLane';
import Menu from '../SideMenu/Menu';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import MousePointers from '../MousePointers/MousePointers';
import { useHistory } from 'react-router';

const KanbanBoard = (props: any): any => {
  const [board, setBoard] = useState({ lanes: [] });
  const [lastBoard, setLastBoard] = useState({ lanes: [] });
  const [selectedCard, setSelectedCard] = useState(Object);
  const [selectedLane, setSelectedLane] = useState(Object);
  const [mousePointer, setMousePointer] = useState({ x: 0, y: 0 });
  const history = useHistory();

  const { theme } = useContext(ThemeContext);

  const boardId: string | undefined = window.location.pathname.split('/').pop();
  const user: any = firebase.auth().currentUser;
  const db: any = firebase.firestore();

  const boardDoc: any = db.collection(`boards`).doc(boardId);
  boardDoc.get().then((doc: any) => props.setBoardName(`Name: ${doc.data().name}`))
    .catch(() => history.push('/notfound'));

  const boardHistoryStr = `boards/${boardId}/history`;

  // Subscribe for board history collection
  useEffect(() => {
    db.collection(boardHistoryStr)
      .orderBy('time', 'desc')
      .limit(1)
      .onSnapshot(function (snapshot: any) {
        snapshot.forEach(function (newBoard: any) {
          setLastBoard(newBoard.data());
          setBoard(newBoard.data());
          console.log(new Date(newBoard.data().time));
        });
      });
  }, [db, boardHistoryStr]);

  const updateDB = (updatedBoard: any): void => {
    const boardHistory = db.collection(boardHistoryStr);
    updatedBoard.time = firebase.firestore.FieldValue.serverTimestamp();
    updatedBoard.user = user.uid;
    boardHistory.doc().set(updatedBoard);
  };

  const handleDataChange = (updatedBoard: any): void => {
    if (JSON.stringify({...updatedBoard, time: null}) === JSON.stringify({...lastBoard, time: null})) {
      return;
    }
    updateDB(updatedBoard);
  };

  const onEditCard = (card: any): void => {
    const updatedBoard: any = { ...board }; //copy because we dont want to render the screen yet
    const lane: any = updatedBoard.lanes.find((o: any) => o.id === card.laneId);
    const cardIndex: number = lane.cards.findIndex(
      (o: any) => o.id === card.id,
    );
    if (cardIndex > -1) {
      const newCard: any = { ...lane.cards[cardIndex], ...card };
      lane.cards[cardIndex] = newCard;
      setSelectedCard(newCard); //update the info in the editor
      updateDB(updatedBoard);
    }
  };

  const onCardClick = (cardId: string, laneId: string): void => {
    if (selectedCard && selectedCard.id === cardId) {
      closeEditor();
      return;
    }

    const lane: any = board.lanes.find((o: any) => o.id === laneId);
    const card: any = lane.cards.find((o: any) => o.id === cardId);
    if (card) {
      //Show card edit
      setSelectedCard(card);

      if (lane) {
        //Show Lane edit
        setSelectedLane(lane);
      }
    }
  };

  const closeEditor = (): void => {
    setSelectedCard(null);
    setSelectedLane(null);
  };

  const onCancelCardEdit = (): void => {
    closeEditor();
  };

  const onEditLane = (lane: any): void => {
    const laneIndex: number = board.lanes.findIndex(
      (o: any) => o.id === lane.id,
    );
    if (laneIndex > -1) {
      const updatedBoard: any = board ? { ...board } : {};
      const newLane: any = { ...updatedBoard.lanes[laneIndex], ...lane };
      updatedBoard.lanes[laneIndex] = newLane;
      setSelectedLane(newLane);
      updateDB(updatedBoard);
    }
  };

  const onMouseMove = (e: any) => {
    setMousePointer({ x: e.clientX, y: e.clientY });
  };

  return (
    <>
      {selectedCard && selectedCard.id && (
        <Menu x={0} y={0}>
          <LaneEditor lane={selectedLane} onEdit={onEditLane} />
          <CardEditor card={selectedCard} onEdit={onEditCard} />
          <button className="btn btn-secondary btn-card-lane-cancel" onClick={onCancelCardEdit}>
            Cancel
          </button>
        </Menu>
      )}
      {Object.keys(board).length > 0 && (
        <Board
          className={`kanbanBoard kanban-background-${theme.background}`}
          style={{
            transform: `scale(${Math.abs(props.zoomBoardLevel) / 100})`,
            paddingTop: `${ 64 / (Math.abs(props.zoomBoardLevel) / 100)}px`
          }}
          data={board}
          draggable={true}
          canAddLanes={true}
          editLaneTitle={true}
          editable={true}
          onDataChange={handleDataChange}
          onCardClick={(cardId: string, metadata: any, laneId: string) =>
            onCardClick(cardId, laneId)
          }
          components={{
            Card: KanbanCard,
            NewCardForm: KanbanNewCard,
            LaneHeader: CustomLaneHeader,
            NewLaneForm: KanbanNewLaneForm,
          }}
          onMouseMove={onMouseMove}
        />
      )}
      <MousePointers
        boardId={boardId}
        mousePointer={mousePointer}
        zoomBoardLevel={props.zoomBoardLevel}
        enableMousePointer={props.enableMousePointer}
      />
    </>
  );
};

export default KanbanBoard;
