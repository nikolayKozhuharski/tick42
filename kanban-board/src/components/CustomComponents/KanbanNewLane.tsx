import React, { useRef, useState } from 'react';
import { GithubPicker } from 'react-color';
import { store } from 'react-notifications-component';

const KanbanNewLaneForm = (props: any): any => {
  const setTitleRef: any = useRef<HTMLInputElement>(null);
  const [laneColor, setLaneColor] = useState('#A8A8A8');
  const { onCancel, t } = props;
  
  const handleAdd = (): void => {
    if (
      setTitleRef.current?.value.length > 0 &&
      setTitleRef.current?.value.length < 25
    ) {
      props.onAdd({
        title: setTitleRef.current?.value,
        laneColor: laneColor,
      });
    } else {
      store.addNotification({
        title: 'Error',
        message: 'Lane title must be between 1 to 25 symbols!',
        type: 'danger', // 'default', 'success', 'info', 'warning', 'danger'
        container: 'top-right', // where to position the notifications
        animationIn: ['animated', 'fadeIn'], // animate.css classes that's applied
        animationOut: ['animated', 'fadeOut'], // animate.css classes that's applied
        dismiss: {
          duration: 3000,
        },
      });
    }
  };

  return (
    <div>
      <div>
        <input
          ref={setTitleRef}
          placeholder={t('placeholder.title')}
          autoFocus
        />
      </div>
      <div>
        <input
          type="color"
          value={laneColor}
          onChange={(e) => setLaneColor(e.target.value)}
        />
        <GithubPicker
          onChange={(color: any) => setLaneColor(color.hex)}
          color={laneColor}
          width="213px"
        />
      </div>
      <div>
        <button onClick={handleAdd}>{t('button.Add lane')}</button>
        <button onClick={onCancel}>{t('button.Cancel')}</button>
      </div>
    </div>
  );
};

export default KanbanNewLaneForm;
