import React from 'react';
import DeleteButton from 'react-trello/dist/widgets/DeleteButton';
import { MovableCardWrapper } from 'react-trello/dist/styles/Base';

const KanbanCard = (props: any): any => {
  
  const clickDelete = (e: any): void => {
    props.onDelete();
    e.stopPropagation(); // not to send the event outside
  };

  return (
    <MovableCardWrapper
      onClick={props.onClick}
      className={props.className}
      style={{
        borderRadius: 6,
        boxShadow: '0 0 6px 1px black',
        marginBottom: 15,
        backgroundColor: 'white',
      }}
    >
      <header
        style={{
          borderBottom: '1px solid #eee',
          paddingBottom: 6,
          marginBottom: 10,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}
      >
        <div style={{ fontSize: 16, fontWeight: 'bold' }}>{props.title}</div>
        <DeleteButton onClick={clickDelete} />
      </header>
      <div style={{ fontSize: 16, color: '#000000', backgroundColor: 'white' }}>
        <div style={{ fontSize: 16 }}>Start: {props.startOn}</div>
        <div style={{ fontSize: 16 }}>Due: {props.dueOn}</div>
        <div style={{ fontSize: 16 }}>Assigne to: {props.assignee}</div>
        <div
          style={{
            fontSize: 16,
            color: '#FFFFFF',
            backgroundColor: props.cardColor,
            width: 160,
            minHeight: 19,
            marginLeft: '34px',
            textShadow: '-1px 0 gray, 0 1px gray, 1px 0 gray, 0 -1px gray',
          }}
        >
          {props.label}
        </div>
      </div>
    </MovableCardWrapper>
  );
};

export default KanbanCard;
