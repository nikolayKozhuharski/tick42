import React, { useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import gitlab from '../../static/img/gitlab.png';
import '../../static/css/Footer.css';
import { ThemeContext } from '../../Context/Theme/ThemeContext';

const Footer = () => {
  const { theme } = useContext(ThemeContext);
  const toGitLab = () => {
    window.open('https://gitlab.com/B.Zhelev/tick42-wb-project', '_blank');
  };

  return (
    <Container
      id="footer"
      fluid={true}
      className={`fixed-bottom bgFooterNav bg-${theme.background}`}
    >
      <Row>
        <Col className="col-md-8">
          <p className="standartText footer">
            COPYRIGHT © 2020 BOYAN ZHELEV, NIKOLAI KOZHUHARSKI AND PETIA
            LATINOVA
          </p>
        </Col>
        <span className="ml-auto text-right footer">
          <span className="standartText">CREATED UNDER THE MIT LICENSE</span>
        </span>
        <span>
          <a id="gitlab" className={`footer-gitlab-ico`} onClick={toGitLab}>
            <img className="gitlab-ico" alt="Not Found" src={gitlab} />
          </a>
        </span>
      </Row>
    </Container>
  );
};

export default Footer;
