import * as firebase from 'firebase'

const initDrawBoard = JSON.stringify({"version":"2.4.3","objects":[{"type":"path","version":"2.4.3","originX":"left","originY":"top","left":-6313,"top":-2287,"width":0,"height":0,"fill":null,"stroke":"black","strokeWidth":0,"strokeDashArray":null,"strokeLineCap":"round","strokeLineJoin":"round","strokeMiterLimit":10,"scaleX":1,"scaleY":1,"angle":0,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"clipTo":null,"backgroundColor":"","fillRule":"nonzero","paintFirst":"fill","globalCompositeOperation":"source-over","transformMatrix":null,"skewX":0,"skewY":0,"path":[["M",-6313,-2287],["L",-6313,-2287]]}], background: "#ffffff"})

const BoardTemplates = {
  Kanban: {
    lanes: [],
    time: firebase.firestore.FieldValue.serverTimestamp(),
    user: firebase.auth().currentUser ? firebase.auth().currentUser!.uid : null
  },
  Database: {
    schema: [],
    time: firebase.firestore.FieldValue.serverTimestamp(),
    user: firebase.auth().currentUser ? firebase.auth().currentUser!.uid : null
  },
  Drawing: {
    canvas: [initDrawBoard],
    time: firebase.firestore.FieldValue.serverTimestamp(),
    user: firebase.auth().currentUser ? firebase.auth().currentUser!.uid : null
  
  },
  MindMap: {
    mindMap: [],
      time: firebase.firestore.FieldValue.serverTimestamp(),
      user: firebase.auth().currentUser ? firebase.auth().currentUser!.uid : null
    },
}
export default BoardTemplates;