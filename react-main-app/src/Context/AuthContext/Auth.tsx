import firebase from 'firebase';
import React, { useEffect, useState, createContext } from 'react'
import FireBaseApp from '../../constant/base';

//To-do add proper types
export const AuthContext = createContext({
    user: null,
    setUser: (val: any) => {}
}as any)

export const AuthProvider = (props: any) => {
    const children: any = props.children;
    const [user, setUser] = useState(JSON.parse(localStorage.getItem('user') as string) || null);
    const [pending, setPending] = useState(true);

    const getUser = async (setUser: any, user: any, uid:string) => {
        const createdBoards: any[] = [];
        const visitedBoards: any[] = [];
        const userFromCollection = firebase.firestore().collection('Users').doc(uid);

        if(!(await userFromCollection.get()).data()){
            const createdUser = FireBaseApp.auth().currentUser;

           await userFromCollection.set({
             username: `${createdUser!.email!.split('@')[0]}`,
             email: createdUser!.email,
             createdBoards: [],
             visitedBoards: []
           });
           localStorage.setItem('user', JSON.stringify({...((await userFromCollection.get()).data() as any), uid}))

           setUser({...((await userFromCollection.get()).data() as any), uid})
        }else{
          const partialUser = (await userFromCollection.get()).data();

          partialUser!
          .createdBoards.map((boardID: string) => {
              firebase
              .firestore()
              .collection('boards')
              .doc(boardID)
              .get()
              .then((ID) => {
                  const newID = ID.data();
                  if (newID) {
                      newID.id = boardID;
                      createdBoards.push(newID);
                  }
              })
          })

          partialUser!
          .visitedBoards.map((boardID: string) => {
              firebase
              .firestore()
              .collection('boards')
              .doc(boardID)
              .get()
              .then((ID) => {
                  visitedBoards.push(ID.data())
              })
          })
          localStorage.setItem('user', JSON.stringify({...partialUser, visitedBoards, createdBoards, uid}))
          setUser({...partialUser, visitedBoards, createdBoards, uid})
        }
      };

    useEffect(() => {
        FireBaseApp.auth().onAuthStateChanged((appUser: any)  => {
            if(appUser){
                getUser(setUser, appUser, appUser.uid)
            }else{
                localStorage.removeItem('user')
                setUser(null)
            }
            setPending(false)
        })

        
    },[]);

    if(pending){
        return (
            <h1>Loading...</h1>
        )
    }

    return (
        <>
        <AuthContext.Provider
        value={{
            user: user,
            setUser: (user: any, id: string) => {getUser(setUser, user, id)}
        }}>
            {children}
        </AuthContext.Provider>
        </>
    )
}