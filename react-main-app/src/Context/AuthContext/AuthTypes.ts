import { ReactNodeArray } from "react";

export type AuthProps = {
    children: ReactNodeArray
  }