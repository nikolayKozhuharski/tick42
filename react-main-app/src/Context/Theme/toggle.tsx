import React, { useState, useEffect, useContext } from 'react'
/*import { ReactComponent as LightIcon } from './icons/light.svg';
import { ReactComponent as DarkIcon } from './icons/dark.svg';
import styled from 'styled-components';
 
const ToggleButton = styled.button`
  border-radius: 30px;
  cursor: pointer;
  display: flex;
  font-size: 0.5rem;
  justify-content: space-between;
  margin: 0 auto;
  overflow: hidden;
  padding: 0.5rem;
  position: relative;
  width: 4rem;
  height: 3rem;
 
  svg {
    height: auto;
    width: 2.5rem;
    transition: all 0.3s linear;
  }
`;
 
const Toggle = ({ theme, toggleTheme }: any) => {
  return (
    <ToggleButton onClick={toggleTheme} >
      <LightIcon />
      <DarkIcon />
    </ToggleButton>
  );
};*/

import DarkModeToggle from "react-dark-mode-toggle";
import { ThemeContext } from './ThemeContext';

const Toggle = () => {
  const { theme, changeTheme} = useContext(ThemeContext)
  const [isDarkMode, setIsDarkMode] = useState(() => theme.background !== 'light');

  useEffect(() => {
    setIsDarkMode(theme.background !== 'light');
  }, [theme])

  console.log('FUCKING CUK BEEE')
  return (
    <DarkModeToggle
      onChange={changeTheme}
      checked={isDarkMode}
      size={66}
    />
  );
};

export default Toggle;