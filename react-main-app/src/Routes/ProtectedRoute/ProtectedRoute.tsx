import firebase from 'firebase';
import React, { useContext, useEffect} from 'react'
import { Route, Redirect } from 'react-router';
import FireBaseApp from '../../constant/base';
import { AuthContext } from '../../Context/AuthContext/Auth';

//Todo add types
const ProtectedRoute = ({component: Component, ...rest}: any) => {
    const {user } = useContext(AuthContext)

    return (
        <Route
          {...rest} render={routeProps => 
            user ? (
              <Component {...routeProps} />
            ) : (
              <Redirect to={"/register"} />
            )
          }
        />
      );
}

export default ProtectedRoute;