import React, { useContext } from 'react';
import { Jumbotron, Container, Card, CardGroup, Button } from 'react-bootstrap';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import { useTranslation, Trans } from 'react-i18next';
import '../../static/css/Home.css';
import { AuthContext } from '../../Context/AuthContext/Auth';

const Home = (props: any) => {
  const { theme } = useContext(ThemeContext);
  const { user } = useContext(AuthContext);
  const { t, i18n } = useTranslation();
  console.log(localStorage.getItem('theme'))

  return (
    <>
      <hr />
      <Container fluid className={`bg-${theme.background}`}>
        <Jumbotron className={`bg-${theme.background}`}>
          <h2><Trans i18nKey='homeWelcome' /></h2>
          <hr />
          <h3><Trans i18nKey='homeWelcomeMsg' /></h3>
        </Jumbotron>

      </Container>

      <hr />

      <Container>
        <span className="rr" />
        <span className="rr-s" />
        <span className="ll-s" />
        <span className="ll" />
        <CardGroup className={'mb-5 my-auto'}>
          <Card key={1} style={{ width: '18rem' }} className={`m-1 bg-${theme.background}`}>
            <Card.Body>
              <Card.Title className="standartText"><Trans i18nKey='homeKanBanCardTitle' /></Card.Title>
              <Card.Text style={{ minHeight: '5rem' }}>
                <Trans i18nKey='homeKanBanCard' />
              </Card.Text>
              <hr />
              {/* <Card.Link href="#"><Trans i18nKey='homeKanBanCardLink' /></Card.Link> */}
              {user && <Button onClick={() => {props.history.push('/profile')}} variant={`outline-${theme.text}`} ><Trans i18nKey='homeCreateLink'/></Button>}
            </Card.Body>
          </Card>

          <Card key={2} style={{ width: '18rem' }} className={`m-1 bg-${theme.background}`}>
            <Card.Body>
              <Card.Title className="standartText"><Trans i18nKey='homeDrawCardTitle' /></Card.Title>
              <Card.Text style={{ minHeight: '5rem' }}>
                <Trans i18nKey='homeDrawCard' />
              </Card.Text>
              <hr />
              {/* <Card.Link href="#"><Trans i18nKey='homeDrawCardLink' /></Card.Link> */}
              {user && <Button onClick={() => {props.history.push('/profile')}} variant={`outline-${theme.text}`} ><Trans i18nKey='homeCreateLink'/></Button>}
            </Card.Body>
          </Card>

          <Card key={3} style={{ width: '18rem' }} className={`m-1 bg-${theme.background}`}>
            <Card.Body>
              <Card.Title className="standartText"><Trans i18nKey='homeMindMapCardTitle' /></Card.Title>
              <Card.Text style={{ minHeight: '5rem' }}>
                <Trans i18nKey='homeMindMapCard' />
              </Card.Text>
              <hr />
              {/* <Card.Link href="#"><Trans i18nKey='homeMindMapCardLink' /></Card.Link> */}
              {user && <Button onClick={() => {props.history.push('/profile')}} variant={`outline-${theme.text}`} ><Trans i18nKey='homeCreateLink'/></Button>}
            </Card.Body>
          </Card>

          <Card key={4} style={{ width: '18rem' }} className={`m-1 bg-${theme.background}`}>
            <Card.Body >
              <Card.Title className="standartText"><Trans i18nKey='homeDBCardTitle' /></Card.Title>
              <Card.Text style={{ minHeight: '5rem' }}>
                <Trans i18nKey='homeDBCard' />
              </Card.Text>
              <hr />
              {/* <Card.Link href="#"><Trans i18nKey='homeDBCardLink' /></Card.Link> */}
              {user && <Button onClick={() => {props.history.push('/profile')}} variant={`outline-${theme.text}`} ><Trans i18nKey='homeCreateLink'/></Button>}

            </Card.Body>
          </Card>
        </CardGroup>
      </Container>
    </>
  )
}

export default Home;