import React, { ReactNode, useContext, useEffect } from 'react';
import clsx from 'clsx';
import FireBaseApp from '../../constant/base';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import AssignmentIcon from '@material-ui/icons/Assignment';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link, useHistory } from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import InfoIcon from '@material-ui/icons/Info';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import Brightness3Icon from '@material-ui/icons/Brightness3';
import LanguageIcon from '@material-ui/icons/Language';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Footer from '../Footer/Footer';
import { store } from 'react-notifications-component';
import { useTranslation } from 'react-i18next';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import { AuthContext } from '../../Context/AuthContext/Auth';


const drawerWidth = 240;


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex'
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      background: 'linear-gradient(0deg, rgba(89,179,196,1) 0%, rgba(112,124,166,1) 47%, rgba(138,87,154,1) 100%)',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      background: 'linear-gradient(0deg, rgba(89,179,196,1) 0%, rgba(112,124,166,1) 47%, rgba(138,87,154,1) 100%)',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

interface LayoutProps {
  children: ReactNode;
}

const Layout = (props: any) => {
  const classes = useStyles();
  const theme2 = useTheme();
  const [open, setOpen] = React.useState(false);
  const history = useHistory();
  const { user } = useContext(AuthContext)

  const {t , i18n} = useTranslation();

  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng);
  };
  
  const {theme, changeTheme} = useContext(ThemeContext)


  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleLogout = async () => {
    localStorage.removeItem('user')
    await FireBaseApp.auth().signOut();
    store.addNotification({
      title: 'Success',
      message: 'You logged out',
      type: 'success',                         // 'default', 'success', 'info', 'warning', 'danger'
      container: 'top-right',                // where to position the notifications
      animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
      animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
      dismiss: {
        duration: 3000
      }
    });
    history.push('/home');
  }

  return (

    <div className={`${classes.root}`}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={`${clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })} bg-${theme.background}`}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon className={`side-nav-text-color-${theme.text}`}/>
          </IconButton>
            <h3 className={`side-nav-text-color-${theme.text}`}>
              WhiteBoard
            </h3>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={`${clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}`}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}}
      >
        <div className={`${classes.toolbar} bg-${theme.background}`}>
          <IconButton onClick={handleDrawerClose} className={`side-nav-text-color-${theme.text}`}>
            {theme2.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List >

          <Link to="/home" style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="Home">
              <ListItemIcon><HomeIcon /></ListItemIcon>
              <ListItemText primary="Home" />
            </ListItem>
          </Link>
          <Link to="/about" style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="About">
              <ListItemIcon><InfoIcon /></ListItemIcon>
              <ListItemText primary="About" />
            </ListItem>
          </Link>

          {user && <Link to="/profile" style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="Profile">
              <ListItemIcon><AccountCircleIcon /></ListItemIcon>
              <ListItemText primary="Profile" />
            </ListItem>
          </Link>}

          {user && <Link to="/boardPage" style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="boardPage">
              <ListItemIcon><AssignmentIcon /></ListItemIcon>
              <ListItemText primary="Public Boards" />
            </ListItem>
          </Link>}

          {/* {user && <Link to="/profile/invitePage" style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="InvitePage">
              <ListItemIcon><BorderColorIcon /></ListItemIcon>
              <ListItemText primary="InvitePage" />
            </ListItem>
          </Link>} */}

          <div style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button onClick={() => { changeLanguage(`${i18n.language === 'en' ? 'bg' : 'en'}`) }}>
              <ListItemIcon><LanguageIcon /></ListItemIcon>
              <ListItemText primary={`${i18n.language === 'en' ? 'English' : 'Bulgarian'}`} />
            </ListItem>
          </div>

          <div style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button onClick={changeTheme}>
              <ListItemIcon>{theme.background === 'light' ? <WbSunnyIcon /> : <Brightness3Icon />}</ListItemIcon>
              <ListItemText primary='Change Theme' />
            </ListItem>
          </div>

          {user && <div style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="Logout" onClick={handleLogout}>
              <ListItemIcon><ExitToAppIcon /></ListItemIcon>
              <ListItemText primary="Logout" />
            </ListItem>
          </div>}

          {!user && <Link to="/login" style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="Login">
              <ListItemIcon><VpnKeyIcon /></ListItemIcon>
              <ListItemText primary="Login" />
            </ListItem>
          </Link>}
          
          {!user && <Link to="/register" style={{ textDecoration: 'none', color: '#0000EE' }}>
            <ListItem button key="Register">
              <ListItemIcon><BorderColorIcon /></ListItemIcon>
              <ListItemText primary="Register" />
            </ListItem>
          </Link>}
        </List>

      </Drawer>
      <main className={classes.content}>
        <div style={{ height: 40, width: 100 }} />       
        {props.children}
        <Footer />
      </main>  
    </div>
  );
}

export default Layout;