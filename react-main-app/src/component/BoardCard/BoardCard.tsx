import React, {useContext, useState } from 'react';
import BorderColorIcon from '@material-ui/icons/BorderColor';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import { Button, Card, ButtonToolbar, Form} from 'react-bootstrap';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import { useGlue } from "@glue42/react-hooks";
import { AuthContext } from '../../Context/AuthContext/Auth';
import firebase from 'firebase';
import { withRouter } from 'react-router-dom';

const BoardCard = (props: any) => {
  const { theme } = useContext(ThemeContext);
  const { user, setUser } = useContext(AuthContext);
  const [isEdit, setIsEdit] = useState(false);
  const [board, setBoard] = useState(props.board)
  const [cardForEdit, setCardForEdit] = useState(undefined);
  const [boardData, setBoardData] = useState({
    isPrivate: false, 
    name: '',
  })
  const firebaseUser = firebase.auth().currentUser;

  const openWindow = useGlue(glue => (name: string, url: string) => {
    const createWindowOptions = {
      name: name,
      url: url,
      width: window.screen.width,
      height: window.screen.height,
    };
    glue.windows.open(name, url, createWindowOptions);
  });
  const handleBoard = (type: string, boardId: string) => {
    openWindow(type, `${type}/${boardId}`);
  };

  const editBoard = async (e: any) => {
    const cardId = e.currentTarget.parentNode?.parentNode?.parentNode.id;


    if(isEdit && cardId === cardForEdit){

    const boardToUpdate = firebase
      .firestore()
      .collection('boards')
      .doc(cardForEdit);

    await boardToUpdate.update(boardData)

    const newUserInfo = await firebase
    .firestore()
    .collection('Users')
    .doc(user.uid)
    .get()

    await setUser(newUserInfo, user.uid)
    setBoard({
      ...board,
      ...boardData
    })

      setIsEdit(false);
      setCardForEdit(undefined);
    }else{
      setBoardData({
        name: board.name,
        isPrivate: board.isPrivate
      })
      setIsEdit(true);
      setCardForEdit(cardId);
    }
  };

  const delBoard = async () => {
    const result = window.confirm("Are you sure you want to delete this board?");
    if (result === false) {
      return;
    }
    const boardToDel = firebase
    .firestore()
    .collection('boards')
    .doc(cardForEdit);


    const newUserInfo =  firebase
    .firestore()
    .collection('Users')
    .doc(user.uid);

    boardToDel.delete()

   const newBoards = user.createdBoards.map((board: any) => {
      if(cardForEdit !== board.id){
        return board.id;
      }
    }).filter((id: string) => typeof id !== 'undefined')

    await newUserInfo.update({
      createdBoards: newBoards
    })

    await setUser((await newUserInfo.get()), user.uid)

    setBoard(null)
  }

  return (
    board &&
    <Card className={`m-1 bg-${theme.background}`} id={board.id}>
    <Card.Body>
    <Card.Text>
    {isEdit && cardForEdit === board.id ? 
    <Form>
  <Form.Control size="sm" type="text" onLoad={() => {console.log('LOADED')}} onChange={(e: any) => {setBoardData({...boardData, name: e.target.value})}} defaultValue={board.name} />
  <Form.Check inline label="Private" onChange={(e: any) => {setBoardData({...boardData, isPrivate: e.target.checked})}} type={'checkbox'} id={`inline-checkbox-1`}  defaultChecked={board.isPrivate}/>
  <Button variant={`outline-${theme.text}`} className={'mt-2'} onClick={delBoard} ><DeleteIcon /></Button>
    </Form>
  : 
  <>
  {`Type: ${board.type}`}
  <br />
  {`Name: ${board.name}`}
  <br />
  {`Owner: ${board.user}`}
  <br />
  {`Board is ${board.isPrivate ? 'Private' : 'Public'}`}
  </>
  }
    </Card.Text>
      <hr className={`bg-${theme.text}`} />
      <ButtonToolbar>

<Button variant={`outline-${theme.text}`} onClick={() => handleBoard(board.type, board.id)}>Open Board</Button>
          {firebaseUser && firebaseUser.uid === board.uid  && 
<>
<Button onClick={editBoard} className={`ml-1`} variant={`outline-${theme.text}`}>{isEdit ? <SaveIcon /> : <BorderColorIcon />}</Button>
{props.board.isPrivate && <Button style={{marginLeft: '5px'}} variant={`outline-${theme.text}`} onClick={() => { localStorage.setItem("typeBoard", board.type);
  return props.history.push(`/profile/invitepage/${props.board.id}`);  }}>Invite</Button>}
</>}
</ButtonToolbar>
    </Card.Body>
  </Card>
  )
}

export default withRouter(BoardCard);