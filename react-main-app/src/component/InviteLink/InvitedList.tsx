import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Switch from '@material-ui/core/Switch';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import BluetoothIcon from '@material-ui/icons/Bluetooth';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
  }),
);

export default function InvitedList(props: any) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(["email"]);
  const inviteEmails = props.inviteEmails;

  const handleToggle = (value: string) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
    console.log(checked)
  };

  return (
    <List subheader={<ListSubheader>Invited emails</ListSubheader>} className={classes.root}>
      { (inviteEmails.length !==0) ? inviteEmails.map((email: string, i: number) =>
        (<ListItem key={i}>
          <ListItemIcon>
            <AlternateEmailIcon />
          </ListItemIcon>
          <ListItemText id="switch-list-label" primary={email} />
        </ListItem>)) : <ListItem>None</ListItem>}
    </List>
  );
}
