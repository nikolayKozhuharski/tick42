import React, { useContext, useState } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import * as firebase from 'firebase'
import { withRouter } from 'react-router-dom';
import moment from "moment";
import BoardTemplates from '../../constant/BoardTemplates';
import { AuthContext } from '../../Context/AuthContext/Auth';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }),
);

function SubmitService(props: any) {

  const { user, setUser } = useContext(AuthContext);
  const classes = useStyles();
  const [isLoading, setLoading] = useState(false);

  const SubmitData = () => {
    setLoading(true);
    const db = firebase.firestore();
    db.collection("boards").add({
      name: props.input,
      type: props.type,
      isPrivate: props.isPrivate,
      user: user.username,
      uid: user.uid,
      creationDate: moment().format(),
      invitedUsersEmail: [],

    })
      .then(function (docRef) {
        let newHistory;
        switch (props.type) {
          case 'kanban':
            newHistory = { ...BoardTemplates.Kanban };
            break;
          case 'database':
            newHistory = { ...BoardTemplates.Database };
            break;
          case 'mindmap':
            newHistory = { ...BoardTemplates.MindMap };
          case 'draw':
            newHistory = { ...BoardTemplates.Drawing};
            break;
          //TODO add other types here
          default:
            console.error("Error creating history: Unknown board type");
            return;
        }
        newHistory.time = firebase.firestore.FieldValue.serverTimestamp();
        docRef.collection("history").add(newHistory);
        db.collection("Users").doc(user.uid).get().then((userDoc) => {
          const userData = userDoc.data();
          console.log(`USER DATA: ${userData}`)
          if (userData) {
            userData.createdBoards.push(docRef.id);
            setUser(userData, userDoc.id)
            db.collection("Users").doc(user.uid).set(userData);
          }

          setLoading(false)
          props.history.push("/profile")
        })

        console.log("Document written with ID: ", docRef, JSON.stringify(user));
      })
      .catch(function (error) {
        console.error("Error adding document: ", error);
      });      

  }

  if (isLoading) {
    return (
      <h1>Loading...</h1>
    )
  }

  return (
    <div className={classes.root}>
      <Button variant="contained" onClick={() => SubmitData()}>Create board</Button>
    </div>)
};

export default withRouter(SubmitService);