import React, { useContext, useState } from "react";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import SubmitService from "./SubmitService";
import { TextField } from "@material-ui/core";
import { ThemeContext } from "../../Context/Theme/ThemeContext";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    paperMargin: {
      marginTop: "20px",
      backgroundColor: "#f8f9fa" 
    },
  }),
);
function CreateBoard() {
  const classes = useStyles();
  const [type, setType] = useState('kanban');
  const [isPrivate, setIsPrivate] = useState(false);
  const [input, setInput] = useState('');
  const { theme } = useContext(ThemeContext);


  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setType(event.target.value as string);
  };

  return (
    < div className = "log-reg-wrapper" >
      <div className="log-reg-container">
        <div className="login-reg-circles">
          <span className="r" />
          <span className="r-s" />
          <span className="l-s" />
          <span className="l" />
        </div>
        <div>
          <div className={`auth-inner authInner bg-${theme.background}`}>
            <br /><br />
          <h1>Create Board</h1>
            <br /><br />
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Board type</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={type}
              onChange={handleChange}
              defaultValue={type}
            >
              <MenuItem value={"draw"}>Draw</MenuItem>
              <MenuItem value={"kanban"}>Kanban</MenuItem>
              <MenuItem value={"mindmap"}>Mind-Map</MenuItem>
              <MenuItem value={"database"}>Database-design</MenuItem>

            </Select>

            <form className={classes.root} noValidate autoComplete="off">
              <TextField id="standard-basic" label="Board name" onChange={(e) => setInput(e.target.value)} placeholder="BoardName" />
            </form>
            <FormControlLabel
              control={
                <Checkbox
                  icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                  checkedIcon={<CheckBoxIcon fontSize="small" />}
                  name="checkedI"
                  onChange={() => setIsPrivate(!isPrivate)}
                />
              }
              label="Private board"
            />
            <SubmitService type={type} isPrivate={isPrivate} input={input} />
          </FormControl>
          </div>
        </div>
      </div>
      </div >)
}


export default CreateBoard;