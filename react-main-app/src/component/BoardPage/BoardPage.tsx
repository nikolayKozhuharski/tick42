import React, { useContext, useEffect, useState } from 'react'
import { Button, Container, Form, Jumbotron, Card, CardColumns } from 'react-bootstrap';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { ContactSupportOutlined } from '@material-ui/icons';
import BoardCard from '../BoardCard/BoardCard';

const BoardPage = () => {
    const db: any = firebase.firestore();
    const { theme } = useContext(ThemeContext);
    const [boards, setBoards] = useState([]);

    useEffect(() => {
        db.collection('boards').orderBy("creationDate", "desc")
            .onSnapshot(function (snapshot: any) {
                const newBoards: any = [];
                snapshot.forEach(function (newBoard: any) {
                    if (newBoard.data().isPrivate) {
                        return;
                    }
                    const data = newBoard.data();
                    data.id = newBoard.id;
                    newBoards.push(data);
                });
                setBoards(newBoards);
            });
    }, [db, setBoards])

    const onFormSubmit = (event: any) => {
        event.preventDefault();   

        const allQueries: any[] = Array.from(event
            .target
            .elements)

       const BoardTypes: any = allQueries
            .filter((el: any) => el.id === 'BoardType')
            .pop()
            .value

        const searchString: any = allQueries
            .filter((el: any) => el.id === 'BoardSearchString')
            .pop()
            .value

        
        setBoards(
            boards.filter((board: any) => {
            if(BoardTypes === 'All' ? false : board.type !== BoardTypes){
                return false;
            }
    
            if(!board.name.toLowerCase().includes(searchString.toLowerCase())){
                return false;
            }

            return true
        }))
    }      

    return (
        <Container style={{paddingBottom: 48}}>
            <span className="rr" />
            <span className="rr-s" />
            <span className="ll-s" />
            <span className="ll" />
        <Jumbotron className={`bg-${theme.background}`}  style={{marginTop: '30px'}}>
            <h1>Public Boards</h1>
        </Jumbotron>

        <Jumbotron className={`bg-${theme.background}`}>
        <h2 className={`text-center`}>Search</h2>
            <Form onSubmit={onFormSubmit}>
                <Form.Group>
                    <Form.Label className="my-1 mr-2">
                        Board Type
                    </Form.Label>
                    <Form.Control
                        as="select"
                        className="my-1 mr-sm-2"
                        custom
                        id='BoardType'
                    >
                        <option value="All">All</option>
                        <option value="kanban">KanBan</option>
                        <option value="mind-map">MindMap</option>
                        <option value="database">DataBase</option>
                        <option value="draw">Whiteboard</option>
                    </Form.Control>

                </Form.Group>

                <Form.Group>
                    <Form.Label sm={2}>
                    Search
                    </Form.Label>

                    <Form.Control id='BoardSearchString' type="text" placeholder="Search..." />
                </Form.Group>

                <Button type="submit" variant={`outline-${theme.text}`} className="my-3 mx-2">Submit</Button>

            </Form>
            
        </Jumbotron>
      
            <hr className={`bg-${theme.text}`}/>
            <Container >
                <h4>Public Boards </h4>
                <CardColumns>
                    {boards.map((board: any) => <BoardCard key={board.id} board={board}/>)}
                </CardColumns>
            </Container>
    </Container>
    )
}

export default BoardPage;