export const bg = {
    translation: {
        homeWelcome: 'Добре дошли в новата ви апликация за планиране !',
        homeWelcomeMsg: 'Надяваме се да ви хареса',

        homeKanBanCard: "Нашият Канбан, напълно оборудван да удовлетвори нуждите на отбора ви",
        homeKanBanCardTitle: 'Канбан',
         homeKanBanCardLink: 'Отвори Канбан',

         homeDrawCard: "Нашата бяла дъска, същото като Paint но по-добро",
         homeDrawCardTitle: 'Бяла Дъска',
         homeDrawCardLink: 'Отвори Бяла дъска',

         homeMindMapCard: "Нашият МайнМап прави нагласянето на мислите ви песен",
         homeMindMapCardTitle: 'МайндМап',
         homeMindMapCardLink: 'Отвори МайндМап',
 
         homeDBCard: "Нашите Бази Данни ви позволяват да си нагласите базата още преди да сте я почнали",
         homeDBCardTitle: 'Бази Данни',
         homeDBCardLink: 'Отвори База Данни',

         homeCreateLink: 'Създайте нов борд',

    }
}