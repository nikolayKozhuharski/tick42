export const en = {
    translation: {
        homeWelcome: 'Welcome to your new all-in one planning app !',
        homeWelcomeMsg: 'We hope you enjoy the experience',
        homeKanBanCard: "This is our KanBan feature, it is fully equiped to suit all of your team's needs ",
        homeKanBanCardTitle: 'Kanban',
        homeKanBanCardLink: 'Go To Kanban',
        
        homeDrawCard: "This is our WhiteBoard feature, just like Paint but better ",
        homeDrawCardTitle: 'WhiteBoard',
        homeDrawCardLink: 'Go To WhiteBoard',

        homeMindMapCard: "This is our MindMap feature, never been easier to map your thoughts",
        homeMindMapCardTitle: 'MindMap',
        homeMindMapCardLink: 'Go To MindMap',

        homeDBCard: "Оur DataBase feature allows you to design Databases before creating them",
        homeDBCardTitle: 'Database',
        homeDBCardLink: 'Go To Database',

        homeCreateLink: 'Create a new board',

    }
}