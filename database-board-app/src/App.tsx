import React, { useState } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import { AuthProvider } from './Context/AuthContext/Auth';
import NotFound from './components/NotFound/NotFound';
import './static/css/App.css';
import ThemeProvider from './Context/Theme/ThemeContext';
import DatabaseBoard from './components/DatabaseBoard/DatabaseBoard';
import Navigation from './components/Navigation/Navigation';
import Footer from './components/Footer/Footer';
import ReactNotifications from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import './Context/Theme/ThemeContext.css';

function App(): any {
  const [zoomBoardLevel, setZoomBoardLevel] = useState(100);
  const [enableMousePointer, setEnableMousePointer] = useState(false);
  const [boardName, setBoardName] = useState(false);

  return (
    <BrowserRouter>
      <ReactNotifications />
      <ThemeProvider>
        <AuthProvider>
          <Navigation
            zoomBoardLevel={zoomBoardLevel}
            setZoomBoardLevel={setZoomBoardLevel}
            enableMousePointer={enableMousePointer}
            setEnableMousePointer={setEnableMousePointer}
            boardName={boardName}
          />
          <div className="App">
            <Switch>
              <Redirect path="/" exact to="/databse" />
              <Route
                path="/database"
                render={(props) => (
                  <DatabaseBoard
                    zoomBoardLevel={zoomBoardLevel}
                    enableMousePointer={enableMousePointer}
                    setBoardName={setBoardName}
                  />
                )}
              />
              <Route path="*" component={NotFound} />
            </Switch>
          </div>
          <Footer />
        </AuthProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
