import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { bg } from './Languages/bg';
import { en } from './Languages/en';

const resources = {
  en: en,
  bg: bg,
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: 'en',

    keySeparator: false, // we do not use keys in form messages.welcome
    ns: ['translation'],
    defaultNS: 'translation',
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
