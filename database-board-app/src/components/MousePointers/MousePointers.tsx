import React, { useEffect, useState } from 'react';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import '../../constants/base';
import '../../static/css/MousePointers.css';

const MousePointers = (props: any) => {
  const [mousePointers, setMousePointers] = useState([]);
  const [lastPointerState, setEnabledPointer] = useState(
    props.enableMousePointer,
  );
  const [myPointer, setMyPointer] = useState({});

  const user: any = firebase.auth().currentUser;

  const db: any = firebase.firestore();
  const boardMouseStr = `boards/${props.boardId}/mouse`;

  const menuHeight = 0;
  const marginXleft = 0;

  useEffect(() => {
    db.collection(boardMouseStr).onSnapshot(function (snapshot: any) {
      const pointers: any = [];
      snapshot.forEach(function (mouse: any) {
        const data: any = mouse.data();

        if (data.enabled && data.userid !== user.uid) {
          const data = mouse.data();
          data.x =
            (data.x - marginXleft) * (props.zoomBoardLevel / 100) + marginXleft;
          data.y =
            (data.y - menuHeight) * (props.zoomBoardLevel / 100) + menuHeight;
          pointers.push(data);
        }
      });
      setMousePointers(pointers);
    });
  }, [db, boardMouseStr, setMousePointers]);

  useEffect(() => {
    const interval = setInterval(() => {
      if (Object.keys(myPointer).length) {
        db.collection(boardMouseStr).doc(user.uid).set(myPointer);
        setMyPointer({}); //if not moving don't send to db
      }
    }, 50); //50ms - send the pointer 20 times per second if it's moved, 20 frames per second shuld be acceptable
    return () => clearInterval(interval);
  }, [db, myPointer]);

  useEffect(() => {
    if (
      !props.enableMousePointer &&
      props.enableMousePointer === lastPointerState
    ) {
      return;
    }
    if (props.enableMousePointer !== lastPointerState) {
      setEnabledPointer(props.enableMousePointer);
    }
    const x: number =
      (props.mousePointer.x - marginXleft) / (props.zoomBoardLevel / 100) +
      marginXleft;
    const y: number =
      (props.mousePointer.y - menuHeight) / (props.zoomBoardLevel / 100) +
      menuHeight;
    setMyPointer({
      x: x,
      y: y,
      time: firebase.firestore.FieldValue.serverTimestamp(),
      user: user.email,
      userid: user.uid,
      enabled: props.enableMousePointer,
    });

    //TEST BEGINN - for offline test. Don't delete! :)
    // const pointers: any = [];
    // pointers.push({
    //  x: x,
    //  y: y,
    //  time: firebase.firestore.FieldValue.serverTimestamp(),
    //  user: user.email,
    //  userid: user.uid,
    //  enabled: true
    // });
    // setMousePointers(pointers);
    // console.log('move plc');
    //TEST END
  }, [db, boardMouseStr, props.mousePointer, props.enableMousePointer]);

  return (
    <>
      {mousePointers.map((o: any) => (
        <>
          <div className="mouse-pointer" style={{ top: o.y, left: o.x }} />
          <div
            className="mouse-pointer-text"
            style={{ top: o.y - 12, left: o.x + 12 }}
          >
            {o.user}
          </div>{' '}
        </>
      ))}
    </>
  );
};

export default MousePointers;
