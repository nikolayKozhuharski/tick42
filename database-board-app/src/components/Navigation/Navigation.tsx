import React, { useContext } from 'react';
import { withRouter } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import Typography from '@material-ui/core/Typography';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import ZoomOutMapIcon from '@material-ui/icons/ZoomOutMap';
import { Trans, useTranslation } from 'react-i18next';
import { ThemeContext } from '../../Context/Theme/ThemeContext';
import pointer_active_light from '../../static/img/pointer_active_light.png';
import pointer_active_dark from '../../static/img/pointer_active_dark.png';
import pointer_disabled_light from '../../static/img/pointer_disabled_light.png';
import pointer_disabled_dark from '../../static/img/pointer_disabled_dark.png';
import WbSunnyIcon from '@material-ui/icons/WbSunny';
import Brightness3Icon from '@material-ui/icons/Brightness3';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import LanguageIcon from '@material-ui/icons/Language';
import ChatButton from '../../components/Chat/ChatButton';
import '../../static/css/Menu.css';

const Navigation = (props: any) => {
  const { theme, changeTheme } = useContext(ThemeContext);

  const { i18n } = useTranslation();

  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng);
  };

  const handleBoardZoomOut = () => {
    if (props.zoomBoardLevel <= 25) return;
    props.setZoomBoardLevel(props.zoomBoardLevel - 25);
  };

  const handleBoardZoomIn = () => {
    if (props.zoomBoardLevel >= 500) return;
    props.setZoomBoardLevel(props.zoomBoardLevel + 25);
  };

  const handleBoardZoomNormal = () => {
    props.setZoomBoardLevel(100);
  };

  const changeImg = (): void => {
    props.setEnableMousePointer(!props.enableMousePointer);
  };

  return (
    <Navbar
      className="navbar-board"
      bg={`${theme.background}`}
      expand="lg"
      style={{ minWidth: 245 }}
    >
      <Navbar.Brand
        style={{ color: `${theme.background === 'light' ? 'black' : 'white'}` }}
        className="navbar-title"
      >
        <Trans i18nKey="databaseTitle" />
      </Navbar.Brand>

      <Navbar.Toggle aria-controls="basic-navbar-nav" />

      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <div style={{ textDecoration: 'none' }}>
            <ListItem
              button
              onClick={() => {
                changeLanguage(`${i18n.language === 'en' ? 'bg' : 'en'}`);
              }}
            >
              <ListItemIcon>
                <LanguageIcon
                  style={{
                    color: `${
                      theme.background === 'light' ? 'black' : 'white'
                    }`,
                  }}
                />
              </ListItemIcon>
              <ListItemText
                style={{
                  color: `${theme.background === 'light' ? 'black' : 'white'}`,
                }}
                primary={`${i18n.language === 'en' ? 'English' : 'Bulgarian'}`}
              />
            </ListItem>
          </div>
        </Nav>
        <Nav className="board-name">
          {props.boardName}
        </Nav>

        <Nav className="ml-auto">
          <span className="mouse-pointer-menu">
            <span>
              <img
                id="img_check"
                src={
                  props.enableMousePointer
                    ? theme.text === 'dark'
                      ? pointer_active_light
                      : pointer_active_dark
                    : theme.text === 'dark'
                    ? pointer_disabled_light
                    : pointer_disabled_dark
                }
                onClick={changeImg}
                className="mouse-pointer-check"
              />
            </span>
          </span>
        </Nav>

        <Nav className="">
          <ListItem button><ChatButton /></ListItem>
        </Nav>

        <Nav>
          <div style={{ textDecoration: 'none' }}>
            <ListItem button onClick={changeTheme}>
              <ListItemIcon>
                {theme.background === 'light' ? (
                  <WbSunnyIcon
                    style={{
                      color: `${
                        theme.background === 'light' ? 'black' : 'white'
                      }`,
                    }}
                  />
                ) : (
                  <Brightness3Icon
                    style={{
                      color: `${
                        theme.background === 'light' ? 'black' : 'white'
                      }`,
                    }}
                  />
                )}
              </ListItemIcon>
              <ListItemText
                style={{
                  color: `${theme.background === 'light' ? 'black' : 'white'}`,
                }}
                primary="Change Theme"
              />
            </ListItem>
          </div>
        </Nav>

        <Nav>
          <Typography
            variant="h6"
            className={`side-nav-text-color-${theme.text}`}
          >
            <Trans i18nKey="zoom" />
          </Typography>
        </Nav>
        <Nav>
          <div>
            <ZoomOutIcon
              onClick={handleBoardZoomOut}
              style={{ marginLeft: 10, cursor: 'pointer' }}
              className={`side-nav-text-color-${theme.text}`}
            />
            <label
              style={{ marginLeft: 2, marginTop: 6 }}
              className={`side-nav-text-color-${theme.text}`}
            >
              {props.zoomBoardLevel < 100
                ? -1 * props.zoomBoardLevel
                : props.zoomBoardLevel}
            </label>
            <ZoomInIcon
              onClick={handleBoardZoomIn}
              style={{ marginLeft: 2, cursor: 'pointer' }}
              className={`side-nav-text-color-${theme.text}`}
            />
            <ZoomOutMapIcon
              onClick={handleBoardZoomNormal}
              style={{ marginLeft: 2, cursor: 'pointer' }}
              className={`side-nav-text-color-${theme.text}`}
            />
          </div>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default withRouter(Navigation);
