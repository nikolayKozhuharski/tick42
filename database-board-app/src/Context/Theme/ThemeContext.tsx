import React, { createContext, useState, useEffect } from 'react'

export const ThemeContext = createContext({
    theme: {
        
    background: 'light',
    text: 'dark',
    SideNavColor: '#F8F9FA'
    }
    ,changeTheme: () => {}
})

const body: HTMLBodyElement | undefined  = Array.from(document.getElementsByTagName('body')).pop()
const getLocalTheme = () => {
    let retVal = localStorage.getItem('theme')
    if(!retVal){
        localStorage.setItem('theme', 'light')
        retVal = 'light'
    }

    return retVal;
};
const setInitThemeVals = (theme: string) => {
    body!.className = `body-color-${theme}`;
    return  {
        background: theme,
        text: `${theme === 'dark' ? 'light' : 'dark'}`,
        SideNavColor: `${theme === 'dark' ? '#343A40' : '#F8F9FA'}`
    };
};
const changeLocalThemeVals = (theme: string) => {
   body!.className = `body-color-${theme === 'dark' ? 'light' : 'dark'}`;
    return  {
        background: `${theme === 'dark' ? 'light' : 'dark'}`,
        text: `${theme === 'dark' ? 'dark' : 'light'}`,
        SideNavColor: `${theme === 'dark' ? '#F8F9FA' : '#343A40'}`,
    }
};
const changeLocalTheme = (theme: string) => `${theme === 'light' ? 'dark' : 'light'}`;

const ThemeProvider = (props: any) => {

    const children: any = props.children;

    
    const [localTheme, setLocalTheme] = useState(getLocalTheme())
    const [appTheme, setAppTheme] = useState(setInitThemeVals(localTheme));

    useEffect(() => {
        setLocalTheme(getLocalTheme())
    }, [])
    
    const changeThemeValue = () => {
        setAppTheme(changeLocalThemeVals(localTheme))
        localStorage.setItem('theme', changeLocalTheme(localTheme))
        setLocalTheme(changeLocalTheme(localTheme))
    };

    window.onstorage = async (e: any) => {
        if((e.newValue === 'light' || e.newValue === 'dark') || (e.oldValue === 'light' || e.oldValue === 'dark')){
            setAppTheme(changeLocalThemeVals(localTheme))
            setLocalTheme(changeLocalTheme(localTheme))
            console.log(e)
        }
    };
    return (
        <ThemeContext.Provider 
        value={{
            theme: appTheme,
            changeTheme: changeThemeValue
        }}>
            {children}
        </ThemeContext.Provider>
    )
}

export default ThemeProvider;