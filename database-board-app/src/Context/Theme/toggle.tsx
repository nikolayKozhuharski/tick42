import React, { useState, useEffect, useContext } from 'react';
import DarkModeToggle from 'react-dark-mode-toggle';
import { ThemeContext } from './ThemeContext';

const Toggle = () => {
  const { theme, changeTheme } = useContext(ThemeContext);
  const [isDarkMode, setIsDarkMode] = useState(
    () => theme.background !== 'light',
  );

  useEffect(() => {
    setIsDarkMode(theme.background !== 'light');
  }, [theme]);

  return (
    <DarkModeToggle onChange={changeTheme} checked={isDarkMode} size={66} />
  );
};

export default Toggle;
