## WHITEBOARD APP

To run this project first install global:  
`npm install --global @glue42/cli-core`

In every microservice run:  
`npm install`  

Create .env file in every microservice folder (react-main-app, kanban-board, database-board-app, whiteboard-app...) and setup your firebase account in the following format:      
```REACT_APP_API_KEY=  
REACT_APP_AUTH_DOMAIN=  
REACT_APP_DATABASE_URL=  
REACT_APP_PROJECT_ID=  
REACT_APP_STORAGE_BUCKET=  
REACT_APP_MESSAGING_SENDER_ID=  
REACT_APP_APP_ID=  
REACT_APP_MEASUREMENT_ID=  
```  

You can change micriservices ports from their package.json files.  

To start the app go in `glue42-configs` folder and run:  
`gluec serve`  

Then go to every microservice folred (react-main-app, kanban-board, database-board-app, whiteboard-app...) and run:  
`npm run start`  

Technical documentation and scheme you can find in `documents` folder.  